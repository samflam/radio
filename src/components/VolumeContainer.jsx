import React, { useState } from 'react';

export default ({ volume, setVolume }) => {
  const flexBasis = String(volume * innerHeight) + "px";
  const [ activeTouchIds, setActiveTouchIds ] = useState([]);

  const calcVolume = pageY => {
    let newVolume = 1 - pageY / innerHeight;
    if (newVolume < 0)
      newVolume = 0;
    if (newVolume > 1)
      newVolume = 1;
    return newVolume;
  }

  const handleDown = event => {
    if (!(event.buttons & 1)) {
      return;
    }
    setVolume(calcVolume(event.pageY));

    const handleMove = event => {
      setVolume(calcVolume(event.pageY));
    };
    document.addEventListener("mousemove", handleMove);

    const handleUp = event => {
      document.removeEventListener("mousemove", handleMove);
      document.removeEventListener("mouseup", handleUp);
    };
    document.addEventListener("mouseup", handleUp);
  };

  const handleTouchStart = event => {
    const newTouches = [ ...event.targetTouches ];
    if (activeTouchIds.length === 0 && newTouches.length === 1) {
      setVolume(calcVolume(newTouches[0].pageY));
    };
    const newTouchIds = newTouches.map(touch => touch.identifier);
    setActiveTouchIds([ ...activeTouchIds, ...newTouchIds ]);
  };

  const handleTouchMove = event => {
    const touches = [ ...event.targetTouches ];
    if (touches.length !== 1) return;
    if (activeTouchIds.length !== 1) return;
    if (touches[0].identifier !== activeTouchIds[0]) return;
    setVolume(calcVolume(touches[0].pageY));
  };

  const handleTouchEnd = event => {
    const removedTouchIds = [...event.changedTouches]
      .map(touch => touch.identifier);
    setActiveTouchIds(
      activeTouchIds
        .filter(id => !removedTouchIds.includes(id))
    );
  };

  const classNames = ["volume-bar"];
  if (activeTouchIds.length > 0) {
    classNames.push("active");
  }

  return <div className="volume-column">
    <button className="volume-button" onMouseDown={handleDown} onTouchStart={handleTouchStart} onTouchMove={handleTouchMove} onTouchEnd={handleTouchEnd} onTouchCancel={handleTouchEnd}>
      <div className="volume-spacer"></div>
      <div id="volume-bar" className={classNames.join(" ")} style={{flexBasis}}></div>
    </button>
  </div>;
};
