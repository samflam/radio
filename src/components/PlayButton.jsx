import React from 'react';

import PlayIcon from '../../static/play.svg';
import PauseIcon from '../../static/pause.svg';

export default ({ audioState, setAudioState }) => {
  const icon = audioState !== "paused"
    ? <PauseIcon />
    : <PlayIcon />
  ;
  const togglePlay = () => {
    if (audioState === "paused") {
      setAudioState("loading");
    }
    else {
      setAudioState("paused");
    }
  };
  return <button className="play-button" onClick={togglePlay}>{icon}</button>;
}
