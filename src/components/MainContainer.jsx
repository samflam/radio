import React, { useState, useEffect } from 'react';
import ControlContainer from './ControlContainer.jsx';
import VolumeContainer from './VolumeContainer.jsx';
import AudioController from './AudioController.jsx';
import { loadVolume, saveVolume, debounce } from '../util.js';

export default () => {
  const [ detailed, setDetailed ] = useState(false);
  const [ volume, setVolume ] = useState(loadVolume() ?? 0.5);
  /* paused: audio is paused
   * loading: audio is nominally trying to play
   * unhealthy: audio is trying to play with issues
   * playing: audio is successfully playing */
  const [ audioState, setAudioState ] = useState("paused");

  const saveVolumeDebounced = debounce(saveVolume);
  const setAndSaveVolume = (volume) => {
    setVolume(volume);
    saveVolumeDebounced(volume);
  }

  // This is needed on mobile browsers for 100vh to always equal the
  // viewport height. Based on:
  //   https://aryedoveidelman.com/fixing_vh_units_on_mobile_once_and_for_all
  useEffect(() => {
    const updateVh = () => {
      const vh = window.innerHeight * 0.01;
      document.documentElement.style.setProperty('--vh', `${vh}px`);
    };
    updateVh();
    const handleResize = event => {
      updateVh();
    };
    const eventTypes = ["resize"];
    //const eventTypes = ["scroll", "resize", "fullscreenchange", "fullscreenerror", "touchcancel", "touchend", "touchmove", "touchstart", "mozbrowserscroll", "mozbrowserscrollareachanged", "mozbrowserscrollviewchanged", "mozbrowserresize", "orentationchange"];
    eventTypes.forEach(eventType => {
      window.addEventListener(eventType, handleResize);
    });
    return () => {
      eventTypes.forEach(eventType => {
        window.removeEventListener(eventType, handleResize);
      });
    };
  }, []);

  return <>
    <AudioController {...{volume, setVolume: setAndSaveVolume, audioState, setAudioState}} />
    <div className="columns">
      <ControlContainer {...{detailed, setDetailed, audioState, setAudioState}} />
      <VolumeContainer {...{volume, setVolume: setAndSaveVolume}} />
    </div>
  </>
};
