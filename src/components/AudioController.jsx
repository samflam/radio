import React, { useEffect } from 'react';



const stream = 'https://fm.samflam.com/raw.mp3';

export default ({ volume, setVolume, audioState, setAudioState }) => {
  useEffect(() => {
    const audio = document.querySelector('audio');
    if (audio.volume != volume) {
      audio.volume = volume;
    }
  }, [volume]);

  useEffect(() => {
    const audio = document.querySelector('audio');
    const handleVolumeChange = event => {
      setVolume(audio.volume);
    };
    audio.addEventListener("volumechange", handleVolumeChange);

    return () => {
      audio.removeEventListener("volumechange", handleVolumeChange);
    };
  }, []);

  useEffect(() => {
    const audio = document.querySelector('audio');
    switch (audioState) {
      case "paused": {
        if (!audio.paused) {
          audio.pause();
        }
        const handlePlay = event => {
          setAudioState("loading");
        };
        audio.addEventListener("play", handlePlay);
        const handlePlaying = event => {
          setAudioState("playing");
        };
        audio.addEventListener("playing", handlePlaying);

        return () => {
          audio.removeEventListener("play", handlePlay);
          audio.removeEventListener("playing", handlePlaying);
        };
      }

      case "loading": {
        if (audio.paused) {
          const nocacheUrl = `${stream}?nocache=${Date.now()}`;
          audio.src = nocacheUrl;
          audio.play().catch(() => {});
        }
        const handlePlaying = event => {
          setAudioState("playing");
        };
        audio.addEventListener("playing", handlePlaying);

        const handlePause = event => {
          setAudioState("paused");
        };
        audio.addEventListener("pause", handlePause);

        const handleProblem = event => {
          setAudioState("unhealthy");
        };
        audio.addEventListener("error", handleProblem);
        audio.addEventListener("stalled", handleProblem);
        audio.addEventListener("ended", handleProblem);

        const timeoutId = setTimeout(() => {
          setAudioState("unhealthy");
        }, 5000);

        return () => {
          clearTimeout(timeoutId);
          audio.removeEventListener("playing", handlePlaying);
          audio.removeEventListener("pause", handlePause);
          audio.removeEventListener("error", handleProblem);
          audio.removeEventListener("stalled", handleProblem);
          audio.removeEventListener("ended", handleProblem);
        };
      }

      case "unhealthy": {
        const handlePause = event => {
          //setAudioState("paused");
        };
        audio.addEventListener("pause", handlePause);

        const handlePlaying = event => {
          setAudioState("playing");
        };
        audio.addEventListener("playing", handlePlaying);

        const retry = () => {
          const nocacheUrl = `${stream}?nocache=${Date.now()}`;
          audio.src = nocacheUrl;
          audio.play().catch(() => {});
        };
        retry();
        const retryId = setInterval(retry, 5000);

        return () => {
          clearInterval(retryId);
          audio.removeEventListener("playing", handlePlaying);
          audio.removeEventListener("pause", handlePause);
        };
      }

      case "playing": {
        const handlePause = event => {
          setAudioState("paused");
        };
        audio.addEventListener("pause", handlePause);

        const handleProblem = event => {
          setAudioState("unhealthy");
        };
        audio.addEventListener("error", handleProblem);
        audio.addEventListener("stalled", handleProblem);
        audio.addEventListener("ended", handleProblem);

        return () => {
          audio.removeEventListener("pause", handlePause);
          audio.removeEventListener("error", handleProblem);
          audio.removeEventListener("stalled", handleProblem);
          audio.removeEventListener("ended", handleProblem);
        };
      }

      default: return;
    }
  }, [audioState]);

  // Uncomment to log most audio events for debugging
  /*
  useEffect(() => {
    const audio = document.querySelector('audio');
    // All event types except progress, timeupdate, durationchange which can be spammy
    const eventTypes = ['abort', 'canplay', 'canplaythrough', 'emptied', 'ended', 'error', 'loadeddata', 'loadedmetadata', 'loadstart', 'pause', 'play', 'playing', 'ratechange', 'resize', 'seeked', 'seeking', 'stalled', 'suspend', 'volumechange', 'waiting'];
    eventTypes.forEach(eventType => {
      audio.addEventListener(eventType, event => {
        const el = document.createElement('div');
        el.innerText = eventType;
        document.body.appendChild(el);
        console.log(eventType, event);
      });
    });
  }, []);
  */

  return <audio preload="none" type="audio/mpeg"></audio>;
};
