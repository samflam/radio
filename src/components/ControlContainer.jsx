import React from 'react';
import InfoButton from './InfoButton.jsx';
import PlayButton from './PlayButton.jsx';
import Details from './Details.jsx';

export default ({ detailed, setDetailed, audioState, setAudioState }) => {
  const details = detailed
    ? <Details />
    : <></>
  ;
  return <div className="control-column">
    <InfoButton {...{detailed, setDetailed}} />
    {details}
    <PlayButton {...{audioState, setAudioState}} />
  </div>;
};
