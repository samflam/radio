import React from 'react';
import style from '../style/application.scss';
import MainContainer from './MainContainer.jsx';

export default () => <MainContainer />;
