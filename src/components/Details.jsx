import React, { useState, useEffect } from 'react';
import defaultArt from '../../static/default-art.png';

export default () => {
  const [ title, setTitle ] = useState(null);
  const [ artist, setArtist ] = useState(null);
  const [ album, setAlbum ] = useState(null);
  const [ track, setTrack ] = useState(null);
  const [ duration, setDuration ] = useState(null);
  const [ imgSrc, setImgSrc ] = useState(defaultArt);
  const [ connected, setConnected ] = useState(false);

  useEffect(() => {
    let ws = new WebSocket('wss://samflam.com/spoke');
    let reconnectId = null;
    const handleMessage = event => {
      event.data.text().then(text => {
        const content = JSON.parse(text.slice(0, -1));
        if (content.head.channel === "radio/metadata") {
          setTitle(content.body.title ?? null);
          setArtist(content.body.artist ?? null);
          setAlbum(content.body.album ?? null);
          setTrack(content.body.track ?? null);
          setDuration(content.body.duration ?? null);
        }
        if (content.head.channel === "radio/albumart") {
          const albumArt = content.body.data ?? null;
          const mimeType = content.body.mime_type ?? null;
          if (albumArt) {
            setImgSrc(`data:${mimeType};base64,${albumArt}`);
          }
          else {
            setImgSrc(defaultArt);
          }
        }
      });
    };

    const handleOpen = event => {
      setConnected(true);
      const msgobj = {
        head: { channel: "-control/subscribe" },
        body: "radio/**",
      };
      const submsg = `${JSON.stringify(msgobj)}\0`;
      ws.send(new Blob([submsg]));
    };

    const handleCloseOrError = event => {
      setConnected(false);
      if (reconnectId) return;
      // Wait before trying to reconnect to prevent spamming attempts if
      // there's a connection issue
      reconnectId = setTimeout(() => {
        ws = new WebSocket('wss://samflam.com/spoke');
        ws.onopen = handleOpen;
        ws.onmessage = handleMessage;
        ws.onclose = handleCloseOrError;
        ws.onerror = handleCloseOrError;
        reconnectId = null;
      }, 1000);
    };

    ws.onopen = handleOpen;
    ws.onmessage = handleMessage;
    ws.onclose = handleCloseOrError;
    ws.onerror = handleCloseOrError;

    return () => {
      // We actually want to close the websocket, so disable the
      // reconnect logic
      ws.onclose = null;
      ws.onerror = null;
      if (reconnectId) {
        clearTimeout(reconnectId);
      }
      ws.close()
    };
  }, []);

  return <div className="details-container">
    <div className="details">
      <div>{artist ?? "Unknown artist"}</div>
      <div>{album ?? "Unknown album"}</div>
      <img id="album-art" src={imgSrc}></img>
      <div>{title ?? "Unknown track"}</div>
    </div>
  </div>;
};
