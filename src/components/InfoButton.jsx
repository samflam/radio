import React from 'react';

import InfoIcon from '../../static/info.svg';

export default ({ detailed, setDetailed }) => {
  const toggleDetailed = () => {
    setDetailed(!detailed);
  };
  return <button className="info-button" onClick={toggleDetailed}>
    <InfoIcon />
  </button>;
};
