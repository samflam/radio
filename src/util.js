export function loadVolume() {
  let volume = localStorage.getItem("volume");

  // No saved volume
  if (volume === null)
    return null;

  volume = +volume;

  // Cannot parse as number
  if (!Number.isFinite(volume))
    return null;

  // Clamp to [0, 1]
  if (volume > 1)
    volume = 1;
  if (volume < 0)
    volume = 0;

  return volume;
}

export function saveVolume(volume) {
  localStorage.setItem("volume", volume);
}

export function debounce(func, delay=1000) {
  let timeoutId = null;
  const inner = (...args) => {
    if (timeoutId) {
      clearTimeout(timeoutId);
    }
    timeoutId = setTimeout(() => {
      func(...args);
      timeoutId = null;
    }, delay);
  }
  return inner;
}
